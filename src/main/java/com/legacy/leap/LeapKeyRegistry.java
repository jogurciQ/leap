package com.legacy.leap;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.client.registry.ClientRegistry;

@OnlyIn(Dist.CLIENT)
public class LeapKeyRegistry
{
	List<KeyBinding> keys;

	public LeapKeyRegistry()
	{
		this.keys = new ArrayList<KeyBinding>();
	}

	public LeapKeyRegistry register(KeyBinding keyBinding)
	{
		if (!keys.contains(keyBinding))
		{
			this.keys.add(keyBinding);
			ClientRegistry.registerKeyBinding(keyBinding);
		}
		return this;
	}
}
