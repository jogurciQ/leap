package com.legacy.leap.client;

import com.legacy.leap.LeapMod;
import com.legacy.leap.network.PacketHandler;
import com.legacy.leap.network.c_to_s.DoubleJumpPacket;

import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class LeapClientEvents
{
	@SubscribeEvent
	public void onKeyPressed(InputEvent.KeyInputEvent event)
	{
		Minecraft mc = Minecraft.getInstance();

		if (mc.player == null)
			return;

		if (LeapMod.Client.LEAP_KEYBIND.isKeyDown() && mc.player.getMotion().getY() < 0)
		{
			PacketHandler.sendToServer(new DoubleJumpPacket());
		}
	}
}
